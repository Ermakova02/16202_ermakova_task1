#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <math.h>
#include <process.h>
#include <time.h>
#include <iomanip> 

using namespace std;

enum Month { Jan = 1, Feb = 2, Mar = 3, Apr = 4, May = 5, Jun = 6, Jul = 7, Aug = 8, Sep = 9, Oct = 10, Nov = 11, Dec = 12 };

Month NumToMonth(int m);
class DateInterval
{
private:
	int diffseconds;
	int diffminutes;
	int diffhours;
	int diffdays;
	int diffyears;
	int diffmonths;
/*	int add_secs;
	int add_mins;
	int add_hours;
	int add_days;
	int add_months;
	int add_years;
	int monthdays[12];*/
public:
	int GetDiffYear() const;
	int GetDiffMonth() const;
	int GetDiffDay() const;
	int GetDiffHour() const;
	int GetDiffMinute() const;
	int GetDiffSecond() const;
	DateInterval();
//	DateInterval(int year, int month, int day, int hour, int minute, int second);
	DateInterval(DateInterval &d);
	DateInterval& operator=(DateInterval &d);
	void SetDiffSec(int diff);
	void SetDiffMin(int diff);
	void SetDiffHour(int diff);
	void SetDiffDay(int diff);
	void SetDiffMonth(int diff);
	void SetDiffYear(int diff);
	string toString() const;
};

class Date
{
private:
	unsigned int seconds;
	unsigned int minutes;
	unsigned int hours;
	unsigned int days;
	Month Months;
	unsigned int years;

	int add_secs;
	int add_mins;
	int add_hours;
	int add_days;
	int add_months;
	int add_years;

	bool invalid_format;

	time_t timer;
	struct tm * ptm;
	int monthdays[12];
	void InitMonthDays();
	string MonthToString(Month m) const;
	int MonthToNum(Month m) const;
	Month NumToMonth(int m) const;
public:
	Date(unsigned int year, Month m, unsigned int day, unsigned int hour, unsigned int minute, unsigned int second);
	Date(unsigned int year, Month m, unsigned int day);
	Date(unsigned int hrs, unsigned int mnts, unsigned int days1);
	Date();
	void Date::AddRestSymbols(int year, int month, int day, int hour, int minute, int second, string &str);
	Date(Date &d);
	Date& operator=(Date &d);
	string toString() const;
	void Adjustseconds();
	void Adjustminutes();
	void Adjusthours();
	void Adjust();
	Date addSeconds(int second);
	Date addMinutes(int minute);
	Date addHours(int hour);
	Date addDays(int day);
	Date addMonths(int month);
	Date addYears(int year);
	void SetMinDate();
	void SetMaxDate();
	unsigned int Getyear() const;
	Month Getmonth() const;
	unsigned int Getday() const;
	unsigned int Gethour() const;
	unsigned int Getminute() const;
	unsigned int GetSecond() const;
	DateInterval getInterval(const Date& another) const;
	Date addInterval(const DateInterval&) const;
	string formatDate(string format);
	void CheckYearFormat(int &year, string &str);
	void CheckMonthFormat(int &month, string &str);
	void CheckDayFormat(int &day, string &str);
	void CheckHourFormat(int &hour, string &str);
	void CheckMinuteFormat(int &minute, string &str);
	void CheckSecondFormat(int &second, string &str);
};

int DateInterval::GetDiffYear() const
{
	return diffyears;
}
int DateInterval::GetDiffMonth() const
{
	return diffmonths;
}
int DateInterval::GetDiffDay() const
{
	return diffdays;
}
int DateInterval::GetDiffHour() const
{
	return diffhours;
}
int DateInterval::GetDiffMinute() const
{
	return diffminutes;
}
int DateInterval::GetDiffSecond() const
{
	return diffseconds;
}
DateInterval::DateInterval(DateInterval &d)
{
	//InitMonthDays();
	diffseconds = d.diffseconds;
	diffminutes = d.diffminutes;
	diffhours = d.diffhours;
	diffdays = d.diffdays;
	diffmonths = d.diffmonths;
	diffyears = d.diffyears;
	/*divadd_secs = d.divadd_secs;
	divadd_mins = d.divadd_mins;
	divadd_hours = d.add_hours;
	add_days = d.add_days;
	add_months = d.add_months;
	add_years = d.add_years;*/
}
DateInterval& DateInterval::operator=(DateInterval &d)
{
	diffseconds = d.diffseconds;
	diffminutes = d.diffminutes;
	diffhours = d.diffhours;
	diffdays = d.diffdays;
	diffmonths = d.diffmonths;
	diffyears = d.diffyears;
	/*add_secs = d.add_secs;
	add_mins = d.add_mins;
	add_hours = d.add_hours;
	add_days = d.add_days;
	add_months = d.add_months;
	add_years = d.add_years;*/
	return *this;
}

void Date::CheckYearFormat(int &year, string &str)
{
	if ((year != 4)&& (year != 0))
	{
		str = "Invalid date format";
		invalid_format = true;
	}
	if (year == 4)
	{
		ostringstream oss;
		oss << years;
//		string tmp_str = oss.str();
		str += oss.str();
	}
	year = 0;
}

void Date::CheckMonthFormat(int &month, string &str)
{
	if ((month != 3) && (month != 2) && (month != 0))
	{
		str = "Invalid date format";
		invalid_format = true;
	}
	if (month == 3)
	{
		switch (Months)
		{
		case Jan: str += "Jan"; break;
		case Feb: str += "Feb"; break;
		case Mar: str += "Mar"; break;
		case Apr: str += "Apr"; break;
		case May: str += "May"; break;
		case Jun: str += "Jun"; break;
		case Jul: str += "Jul"; break;
		case Aug: str += "Aug"; break;
		case Sep: str += "Sep"; break;
		case Oct: str += "Oct"; break;
		case Nov: str += "Nov"; break;
		case Dec: str += "Dec"; break;
		}
		
	}
	if (month == 2)
	{
		ostringstream oss;
		oss << MonthToNum(Months);
		str += oss.str();
	}
	month = 0;
}
void Date::CheckDayFormat(int &day, string &str)
{
	if ((day != 2) && (day != 0))
	{
		str = "Invalid date format";
		invalid_format = true;
	}
	if (day == 2)
	{
		ostringstream oss;
		oss << days;
		//		string tmp_str = oss.str();
		str += oss.str();
	}
	day = 0;
}
void Date::CheckHourFormat(int &hour, string &str)
{
	if ((hour != 2) && (hour != 0))
	{
		str = "Invalid date format";
		invalid_format = true;
	}
	if (hour == 2)
	{
		ostringstream oss;
		oss << hours;
		//		string tmp_str = oss.str();
		str += oss.str();
	}
	hour = 0;
}
void Date::CheckMinuteFormat(int &minute, string &str)
{
	if ((minute != 2) && (minute != 0))
	{
		str = "Invalid date format";
		invalid_format = true;
	}
	if (minute == 2)
	{
		ostringstream oss;
		oss << minutes;
		//		string tmp_str = oss.str();
		str += oss.str();
	}
	minute = 0;
}
void Date::CheckSecondFormat(int &second, string &str)
{
	if ((second != 2) && (second != 0))
	{
		str = "Invalid date format";
		invalid_format = true;
	}
	if (second == 2)
	{
		ostringstream oss;
		oss << seconds;
		//		string tmp_str = oss.str();
		str += oss.str();
	}
	second = 0;
}

void Date::AddRestSymbols(int year, int month, int day, int hour, int minute, int second, string &str)
{
	if ((year == 1)||(year > 4)) str = "Invalid date format";//str += "Y";
	if (year == 2) str = "Invalid date format";//str += "YY";
	if (year == 3) str = "Invalid date format";//str += "YYY";
	if ((month == 1) || (month > 3)) str = "Invalid date format";//str += "M";
	if ((day == 1) || (day > 2)) str = "Invalid date format";//str += "D";
	if ((hour == 1) || (hour > 2)) str = "Invalid date format";//str += "h";
	if ((minute == 1) || (minute > 2)) str = "Invalid date format";//str += "m";
	if ((second == 1) || (second > 2)) str = "Invalid date format";//str += "s";
}

string Date::formatDate(string format)
{
	invalid_format = false;
	string out_str = "";
	int year_num=0;
	int month_num=0;
	int day_num=0;
	int hour_num=0;
	int minute_num=0;
	int second_num=0;
	int j = 0;
	for (int i = 0; i < format.length(); i++)
	{
		switch (format[i])
		{
		case 'Y':
		{
			CheckMonthFormat(month_num, out_str);
			CheckDayFormat(day_num, out_str);
			CheckHourFormat(hour_num, out_str);
			CheckMinuteFormat(minute_num, out_str);
			CheckSecondFormat(second_num, out_str);
			year_num++;
			break;
		}
		case 'M':
		{
			CheckYearFormat(year_num, out_str);
			CheckDayFormat(day_num, out_str);
			CheckHourFormat(hour_num, out_str);
			CheckMinuteFormat(minute_num, out_str);
			CheckSecondFormat(second_num, out_str);
			month_num++;
			break;
		}
		case 'D':
		{
			CheckYearFormat(year_num, out_str);
			CheckMonthFormat(month_num, out_str);
			CheckHourFormat(hour_num, out_str);
			CheckMinuteFormat(minute_num, out_str);
			CheckSecondFormat(second_num, out_str);
			day_num++;
			break;
		}
		case 'h':
		{
			CheckYearFormat(year_num, out_str);
			CheckMonthFormat(month_num, out_str);
			CheckDayFormat(day_num, out_str);
			CheckMinuteFormat(minute_num, out_str);
			CheckSecondFormat(second_num, out_str);
			hour_num++;
			break;
		}
		case 'm':
		{
			CheckYearFormat(year_num, out_str);
			CheckMonthFormat(month_num, out_str);
			CheckDayFormat(day_num, out_str);
			CheckHourFormat(hour_num, out_str);
			CheckSecondFormat(second_num, out_str);
			minute_num++;
			break;
		}
		case 's':
		{
			CheckYearFormat(year_num, out_str);
			CheckMonthFormat(month_num, out_str);
			CheckDayFormat(day_num, out_str);
			CheckHourFormat(hour_num, out_str);
			CheckMinuteFormat(minute_num, out_str);
			second_num++;
			break;
		}
		default:
		{
			CheckYearFormat(year_num, out_str);
			CheckMonthFormat(month_num, out_str);
			CheckDayFormat(day_num, out_str);
			CheckHourFormat(hour_num, out_str);
			CheckMinuteFormat(minute_num, out_str);
			CheckSecondFormat(second_num, out_str);
			out_str += format[i];
		}
		}
		if (invalid_format)
		{
			invalid_format = false;
			return out_str;
		}
		
	}
	if (year_num == 4)
	{
		ostringstream oss;
		oss << years;
		out_str += oss.str();
		year_num = 0;
	}
	if (month_num == 3)
	{
		switch (Months)
		{
		case Jan: out_str += "Jan"; break;
		case Feb: out_str += "Feb"; break;
		case Mar: out_str += "Mar"; break;
		case Apr: out_str += "Apr"; break;
		case May: out_str += "May"; break;
		case Jun: out_str += "Jun"; break;
		case Jul: out_str += "Jul"; break;
		case Aug: out_str += "Aug"; break;
		case Sep: out_str += "Sep"; break;
		case Oct: out_str += "Oct"; break;
		case Nov: out_str += "Nov"; break;
		case Dec: out_str += "Dec"; break;
		}
		month_num = 0;
	}
	if (month_num == 2)
	{
		// ��������� MM
		ostringstream oss;
		oss << MonthToNum(Months);
		out_str += oss.str();
		month_num = 0;
	}
	if (day_num == 2)
	{
		ostringstream oss;
		oss << days;
		out_str += oss.str();
		day_num = 0;
	}
	if (hour_num == 2)
	{
		ostringstream oss;
		oss << hours;
		out_str += oss.str();
		hour_num = 0;
	}
	if (minute_num == 2)
	{
		ostringstream oss;
		oss << minutes;
		out_str += oss.str();
		minute_num = 0;
	}
	if (second_num == 2)
	{
		ostringstream oss;
		oss << seconds;
		out_str += oss.str();
		second_num = 0;
	}
	AddRestSymbols(year_num, month_num, day_num, hour_num, minute_num, second_num, out_str);
	return out_str;
}
////////////////////////////////////
////////////////////////////////////
DateInterval::DateInterval()
{
	diffseconds=0;
	diffminutes=0;
	diffhours=0;
	diffdays=0;
	diffyears=0;
	diffmonths=0;
}
void DateInterval::SetDiffSec(int diff)
{
	diffseconds = diff;
}
void DateInterval::SetDiffMin(int diff)
{
	diffminutes = diff;
}
void DateInterval::SetDiffHour(int diff)
{
	diffhours = diff;
}
void DateInterval::SetDiffDay(int diff)
{
	diffdays = diff;
}
void DateInterval::SetDiffMonth(int diff)
{
	diffmonths = diff;
}
void DateInterval::SetDiffYear(int diff)
{
	diffyears = diff;
}



DateInterval Date::getInterval(const Date& another) const
{
	DateInterval d1;
	d1.SetDiffSec((int)seconds - (int)another.GetSecond());
	d1.SetDiffMin((int)minutes - (int)another.Getminute());
	d1.SetDiffHour((int)hours - (int)another.Gethour());
	d1.SetDiffDay((int)days - (int)another.Getday());
	d1.SetDiffMonth(MonthToNum(Months)-MonthToNum(another.Getmonth()));
	d1.SetDiffYear((int)years - (int)another.Getyear());
	return d1;
}

Date Date::addInterval(const DateInterval& another) const
{
	Date d1(years, Months, days, hours, minutes, seconds);
	d1 = d1.addSeconds(another.GetDiffSecond());
	d1 = d1.addMinutes(another.GetDiffMinute());
	d1 = d1.addHours(another.GetDiffHour());
	d1 = d1.addDays(another.GetDiffDay());
	d1 = d1.addMonths(another.GetDiffMonth());
	d1 = d1.addYears(another.GetDiffYear());
	return d1;
}

unsigned int Date::Getyear() const
{
	return years;
}
Month Date::Getmonth() const
{
	return Months;
}
unsigned int Date::Getday() const
{
	return days;
}
unsigned int Date::Gethour() const
{
	return hours;
}
unsigned int Date::Getminute() const
{
	return minutes;
}
unsigned int Date::GetSecond() const
{
	return seconds;
}
void Date::InitMonthDays()
{
	monthdays[0] = 31;
	monthdays[1] = 28;
	monthdays[2] = 31;
	monthdays[3] = 30;
	monthdays[4] = 31;
	monthdays[5] = 30;
	monthdays[6] = 31;
	monthdays[7] = 31;
	monthdays[8] = 30;
	monthdays[9] = 31;
	monthdays[10] = 30;
	monthdays[11] = 31;
}

void Date::SetMinDate()
{
	years = 1;
	Months = Jan;
	days = 1;
	hours = 0;
	minutes = 0;
	seconds = 0;
}

void Date::SetMaxDate()
{
	years = 9999;
	Months = Dec;
	days = 31;
	hours = 23;
	minutes = 59;
	seconds = 59;
}

void Date::Adjustseconds()
{
	if (add_secs >= 0)
	{
		seconds += add_secs;
		while (seconds >= 60)
		{
			add_mins++;
			seconds -= 60;
		}
	}
	else
	{
		while ((int)seconds + add_secs < 0)
		{
			add_mins--;
			add_secs += 60;
		}
		seconds = seconds - (unsigned int)(-add_secs);
	}
	add_secs = 0;
}
void Date::Adjustminutes()
{
	if (add_mins >= 0)
	{
		minutes += add_mins;
		while (minutes >= 60)
		{
			add_hours++;
			minutes -= 60;
		}
	}
	else
	{
		while ((int)minutes + add_mins < 0)
		{
			add_hours--;
			add_mins += 60;
		}
		minutes = minutes - (unsigned int)(-add_mins);
	}
	add_mins = 0;
}

void Date::Adjusthours()
{
	if (add_hours >= 0)
	{
		hours += add_hours;
		while (hours >= 24)
		{
			add_days++;
			hours -= 24;
		}
	}
	else
	{
		while ((int)hours + add_hours < 0)
		{
			add_days--;
			add_hours += 24;
		}
		hours = hours - (unsigned int)(-add_hours);
	}
	add_hours = 0;
}
void Date::Adjust()
{
	if ((years % 4 == 0) || (years % 400 == 0)) monthdays[1] = 29;
	else monthdays[1] = 28;
	if (add_days > 0)
	{
		days += add_days;
		while (days >= monthdays[(unsigned int)Months - 1])
		{
			days -= monthdays[(unsigned int)Months - 1];
			if ((unsigned int)Months == 12)
			{
				Months = Jan;
				years++;
				if (years > 9999)
				{
					SetMaxDate();
					add_days = 0;
					add_months = 0;
					add_years = 0;
					return;
				}
				if ((years % 4 == 0) || (years % 400 == 0)) monthdays[1] = 29;
				else monthdays[1] = 28;
			}
			else
				Months = NumToMonth((unsigned int)Months + 1);
		}
	}
	else if (add_days < 0)
	{
		while ((int)days + add_days < 1)
		{
			if (Months == Jan) add_days += monthdays[11];
			else add_days += monthdays[(unsigned int)Months - 2];
			if (Months == Jan)
			{
				Months = Dec;
				years--;
				if (years < 1)
				{
					SetMinDate();
					add_days = 0;
					add_months = 0;
					add_years = 0;
					return;
				}
				if ((years % 4 == 0) || (years % 400 == 0)) monthdays[1] = 29;
				else monthdays[1] = 28;
			}
			else
				Months = NumToMonth((unsigned int)Months - 1);
		}
		days = days + add_days;
	}
	add_days = 0;
	if (add_months > 0)
	{
		while ((unsigned int)Months + add_months > 12)
		{
			years++;
			if (years > 9999)
			{
				SetMaxDate();
				add_months = 0;
				add_years = 0;
				return;
			}
			add_months -= 12;
		}
		if ((days > monthdays[(unsigned int)Months + add_months - 1]) || (days == monthdays[(unsigned int)Months - 1]))
			days = monthdays[(unsigned int)Months + add_months - 1];
		Months = NumToMonth((unsigned int)Months + add_months);
	}
	else
	{
		while ((int)Months + add_months < 1)
		{
			years--;
			if (years < 1)
			{
				SetMinDate();
				add_months = 0;
				add_years = 0;
				return;
			}
			add_months += 12;
		}
		if ((days > monthdays[(unsigned int)Months + add_months - 1]) || (days == monthdays[(unsigned int)Months - 1]))
			days = monthdays[(unsigned int)Months + add_months - 1];
		Months = NumToMonth((unsigned int)Months + add_months);
	}
	add_months = 0;
	if (add_years >= 0)
	{
		if (years + add_years > 9999)
		{
			SetMaxDate();
			add_years = 0;
			return;
		}
		years += add_years;
	}
	else
	{
		if ((int)years + add_years < 1)
		{
			SetMinDate();
			add_years = 0;
			return;
		}
		years = years - (unsigned int)(-add_years);
	}
	add_years = 0;
}

Date Date::addSeconds(int second)
{
	add_secs = second;
	Date data = *this;
	add_secs = 0;
	data.Adjustseconds();
	data.Adjustminutes();
	data.Adjusthours();
	data.Adjust();
	return data;
}
Date Date::addMinutes(int minute)
{
	add_mins = minute;
	Date data = *this;
	add_mins = 0;
	data.Adjustminutes();
	data.Adjusthours();
	data.Adjust();
	return data;
}
Date Date::addHours(int hour)
{
	add_hours = hour;
	Date data = *this;
	add_hours = 0;
	data.Adjusthours();
	data.Adjust();
	return data;
}
Date Date::addDays(int day)
{
	add_days = day;
	Date data = *this;
	add_days = 0;
	data.Adjust();
	return data;
}
Date Date::addMonths(int month)
{
	add_months = month;
	Date data = *this;
	add_months = 0;
//	int month1;
//	month1=MonthToNum(data.Months) + month;
//	data.Months = NumToMonth(month1);
	data.Adjust();
	return data;
}
Date Date::addYears(int year)
{
	Date data = *this;
	if (year >= 0)
	{
		data.years += static_cast<unsigned int>(year);
		if (data.years > 9999) data.SetMaxDate();
	}
	else 
	{
		year = -year;
		if (static_cast<unsigned int>(year) <= data.years + 1) data.years -= static_cast<unsigned int>(year);
		else data.SetMinDate();
	}
	return data;
}

Date::Date(unsigned int year, Month m, unsigned int day, unsigned int hour, unsigned int minute, unsigned int second)
{
	InitMonthDays();
	invalid_format = true;
	add_secs = second;
	add_mins = minute;
	add_hours = hour;
	add_days = day;
	add_months = MonthToNum(m) - 1;
	add_years = year;

	seconds = 0;
	minutes = 0;
	hours = 0;
	days = 0;
	Months = Jan;
	Adjustseconds();
	Adjustminutes();
	Adjusthours();
	Adjust();
}

Date::Date(Date &d)
{
	InitMonthDays();
	invalid_format = d.invalid_format;
	seconds = d.seconds;
	minutes = d.minutes;
	hours = d.hours;
	days = d.days;
	Months = d.Months;
	years = d.years;
	add_secs = d.add_secs;
	add_mins = d.add_mins;
	add_hours = d.add_hours;
	add_days = d.add_days;
	add_months = d.add_months;
	add_years = d.add_years;
}

Date& Date::operator=(Date &d)
{
	invalid_format = d.invalid_format;
	seconds = d.seconds;
	minutes = d.minutes;
	hours = d.hours;
	days = d.days;
	Months = d.Months;
	years = d.years;
	add_secs = d.add_secs;
	add_mins = d.add_mins;
	add_hours = d.add_hours;
	add_days = d.add_days;
	add_months = d.add_months;
	add_years = d.add_years;
	return *this;
}

Date::Date()
{
	InitMonthDays();
	time(&timer);
	ptm = gmtime(&timer);
	years = ptm->tm_year+1900;
	days = ptm->tm_mday;
	minutes = ptm->tm_min;
	hours = ptm->tm_hour;
	seconds = ptm->tm_sec;
	Months = NumToMonth(ptm->tm_mon + 1);
	add_secs = 0;
	add_mins = 0;
	add_hours = 0;
	add_days = 0;
	add_months = 0;
	add_years = 0;
	invalid_format = true;
}
Date::Date(unsigned int year, Month m, unsigned int day)
{
	InitMonthDays();
	invalid_format = true;
	add_secs = 0;
	add_mins = 0;
	add_hours = 0;
	add_days = day;
	add_months = MonthToNum(m) - 1;
	add_years = year;
	years = 0;
	days = 0;
	Months = Jan;
	hours = 0;
	minutes = 0;
	seconds = 0;
	Adjustseconds();
	Adjustminutes();
	Adjusthours();
	Adjust();
}

Date::Date(unsigned int hrs, unsigned int mnts, unsigned int days1)
{
	InitMonthDays();
	invalid_format = true;
	hours = hrs;
	minutes = mnts;
	days = days1;
	time(&timer);
	ptm = gmtime(&timer);
	years = ptm->tm_year+1900;
	Months = NumToMonth(ptm->tm_mon+1);
	add_secs = 0;
	add_mins = 0;
	add_hours = 0;
	add_days = 0;
	add_months = 0;
	add_years = 0;
}

Month Date::NumToMonth(int m) const
{
	Month mnth = Jan;
	switch (m)
	{
	case 1: mnth = Jan; break;
	case 2: mnth = Feb; break;
	case 3: mnth = Mar; break;
	case 4: mnth = Apr; break;
	case 5: mnth = May; break;
	case 6: mnth = Jun; break;
	case 7: mnth = Jul; break;
	case 8: mnth = Aug; break;
	case 9: mnth = Sep; break;
	case 10: mnth = Oct; break;
	case 11: mnth = Nov; break;
	case 12: mnth = Dec; break;
	}
	return mnth;
}

Month NumToMonth(int m)
{
	Month mnth = Jan;
	switch (m)
	{
	case 1: mnth = Jan; break;
	case 2: mnth = Feb; break;
	case 3: mnth = Mar; break;
	case 4: mnth = Apr; break;
	case 5: mnth = May; break;
	case 6: mnth = Jun; break;
	case 7: mnth = Jul; break;
	case 8: mnth = Aug; break;
	case 9: mnth = Sep; break;
	case 10: mnth = Oct; break;
	case 11: mnth = Nov; break;
	case 12: mnth = Dec; break;
	}
	return mnth;
}

string Date::MonthToString(Month m) const
{
	string mnth;
	switch (Months)
	{
	case Jan: mnth = "Jan"; break;
	case Feb: mnth = "Feb"; break;
	case Mar: mnth = "Mar"; break;
	case Apr: mnth = "Apr"; break;
	case May: mnth = "May"; break;
	case Jun: mnth = "Jun"; break;
	case Jul: mnth = "Jul"; break;
	case Aug: mnth = "Aug"; break;
	case Sep: mnth = "Sep"; break;
	case Oct: mnth = "Oct"; break;
	case Nov: mnth = "Nov"; break;
	case Dec: mnth = "Dec"; break;
	}
	return mnth;
}

int Date::MonthToNum(Month m) const
{
	int mnth=0;
	switch (m)
	{
	case Jan: mnth = 1; break;
	case Feb: mnth = 2; break;
	case Mar: mnth = 3; break;
	case Apr: mnth = 4; break;
	case May: mnth = 5; break;
	case Jun: mnth = 6; break;
	case Jul: mnth = 7; break;
	case Aug: mnth = 8; break;
	case Sep: mnth = 9; break;
	case Oct: mnth = 10; break;
	case Nov: mnth = 11; break;
	case Dec: mnth = 12; break;
	}
	return mnth;
}

string Date::toString() const
{
	ostringstream oss;
	string mnths("");
	mnths = MonthToString(Months);
	oss << setfill('0')<< setw(4)<< years << "-" << setfill('0') << setw(3)<< mnths << "-" << setfill('0') << setw(2)<<days<< " "<< setfill('0') << setw(2) <<hours<<":" << setfill('0') << setw(2) << minutes << ":" << setfill('0') << setw(2) << seconds << endl;
	string s = oss.str();
	return s;
}
string DateInterval::toString() const
{
	ostringstream oss;
	string mnths("");
	oss << setfill('0') << setw(4) << diffyears << "-" << setfill('0') << setw(2) << diffmonths << "-" << setfill('0') << setw(2) << diffdays << " " << setfill('0') << setw(2) << diffhours << ":" << setfill('0') << setw(2) << diffminutes << ":" << setfill('0') << setw(2) << diffseconds << endl;
	string s = oss.str();
	return s;
}

void PrintInfo()
{
	cout <<"Press 1 to check constructor Date(unsigned int year, Month m, unsigned int day); " << endl;
	cout << "Press 2 to check constructor Date(unsigned int hrs, unsigned int mnts, unsigned int days1); " << endl;
	cout << "Press 3 to check operations Date addSeconds(int second), Date addMinutes(int minute), Date addHours(int hour), Date addDays(int day), Date addMonths(int month), Date addYears(int year); " << endl;
	cout << "Press 4 to check Date copy constructor; " << endl;
	cout << "Press 5 to check constructor Date(unsigned int year, Month m, unsigned int day, unsigned int hour, unsigned int minute, unsigned int second); " << endl;
	cout << "Press 6 to check selectors; " << endl;
	cout << "Press 7 to check DateInterval copy constructor;" << endl;
	cout << "Press 8 to check DateInterval getInterval(const Date& another) const and Date addInterval(const DateInterval&) const;" << endl;
	cout << "Press 9 to check string formatDate(string format);" << endl;
	cout << "Press 10 to exit." << endl;
}

int main()
{
/*	ostringstream oss;
	oss << 123;
	string str = oss.str();
	cout << str << endl;*/
	time_t timer;
	struct tm * ptm;
	time(&timer);
	ptm = gmtime(&timer);
	unsigned int day, year, month1, hour, minute, second;
	Month month;
	int option = 0;
	while (option != 10)
	{
		PrintInfo();
		cin >> option;
		switch (option)
		{
		case 1:
		{
			cout << "Input year (1-9999): ";
			cin >> year;
			cout << "Input month (1-12): ";
			cin >> month1;
			cout << "Input day (1-31): ";
			cin >> day;
			Date d2(year, NumToMonth(month1), day);
			cout << "Date: ";
			cout << d2.toString() << endl;
		} break;
		case 2:
		{cout << "Input hour: ";
		cin >> hour;
		cout << "Input minutes: ";
		cin >> minute;
		cout << "Input day: ";
		cin >> day;
		Date d2(hour, minute, day);
		cout << "Date: ";
		cout << d2.toString() << endl;
		break;
		}
		case 3:
		{Date theDate;
		cout << "Current Date: " << theDate.toString() << endl;
		cout << "Input second for addSeconds: ";
		cin >> second;
		theDate=theDate.addSeconds(second);
		cout << "Input month for addMinutes: ";
		cin >> minute;
		theDate=theDate.addMinutes(minute);
		cout << "Input month for addHours: ";
		cin >> hour;
		theDate=theDate.addHours(hour);
		cout << "Input month for addDays: ";
		cin >> day;
		theDate = theDate.addDays(day);
		cout << "Input month for addMonths: ";
		cin >> month1;
		theDate=theDate.addMonths(month1);
		cout << "Input month for addYears: ";
		cin >> year;
		theDate=theDate.addYears(year);
		// ���� m ��� aadMonth.
		// cin >> m
		cout << "New Date: " << theDate.toString() << endl;
		// currDate.aadMonth(m);
		// cout << "����� ����"
		// currDate.toString();
		break;
		}
		case 4:
		{cout << "Copy constructor test: " << endl;
		Date d1;
		Date d3 = d1;
		cout << d1.toString() << endl;
		cout << d3.toString() << endl;
		break;
		}
		case 5:
		{cout << "Input year (1-9999): ";
		cin >> year;
		cout << "Input month (1-12): ";
		cin >> month1;
		cout << "Input day (1-31): ";
		cin >> day;
		cout << "Input hour: ";
		cin >> hour;
		cout << "Input minutes: ";
		cin >> minute;
		cout << "Input seconds: ";
		cin >> second;
		Date d2(year, NumToMonth(month1-1), day, hour, minute, second);
		cout << d2.toString() << endl;
		break;
		}
		case 6:
		{ Date d1;
		year = d1.Getyear();
		month = d1.Getmonth();
		day = d1.Getday(); 
		hour = d1.Gethour();
		minute = d1.Getminute();
		second = d1.GetSecond();
		cout << year << endl << month << endl << day << endl << hour << endl << minute << endl << second << endl;
		break;
		}
		case 7:
		{
			cout << "Copy constructor test: " << endl;
			DateInterval d1;
			DateInterval d3 = d1;
			cout << d1.toString() << endl;
			cout << d3.toString() << endl;
			break;
		}
		case 8:
		{
			Date d1,d3;
			Date d2 = d1;
			DateInterval m1;
			cout << "Input second for addSeconds: ";
			cin >> second;
			d2 = d2.addSeconds(second);
			cout << "Input month for addMinutes: ";
			cin >> minute;
			d2 = d2.addMinutes(minute);
			cout << "Input month for addHours: ";
			cin >> hour;
			d2 = d2.addHours(hour);
			cout << "Input month for addDays: ";
			cin >> day;
			d2 = d2.addDays(day);
			cout << "Input month for addMonths: ";
			cin >> month1;
			d2 = d2.addMonths(month1);
			cout << "Input month for addYears: ";
			cin >> year;
			d2 = d2.addYears(year);
			cout << "Current Date: " << d1.toString() << endl;
			cout << "New Date: " << d2.toString() << endl;
			m1 = d2.getInterval(d1);
			d3 = d1.addInterval(m1);
			cout << "Interval: " << m1.toString() << endl;
			cout << "Date After Add Interval: " << d3.toString() << endl;
			break;
		}
		case 9:
		{
			Date d1;
			string str, format;
			cin >> format;
			str = d1.formatDate(format);
			cout << str << endl;
			break;
		}
		}
	}
	system("pause");
}
